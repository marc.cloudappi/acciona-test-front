import Vue from 'vue'
import VueRouter from 'vue-router'
import UsersList from '../views/users/list/UsersList.vue';
import UsersDetails from '../views/users/details/UsersDetails.vue';
import UsersFavorites from '../views/users/favorites/UsersFavorites.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'UsersList',
    component: UsersList
  },
  {
    path: '/user/:email',
    name: 'UsersDetails',
    component: UsersDetails
  },
  {
    path: '/favorites',
    name: 'UsersFavorites',
    component: UsersFavorites
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
