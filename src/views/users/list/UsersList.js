import usersService from '../../../services/usersService.js';
import usersTable from '../../../components/users/usersTable.vue';
import moment from 'moment';

export default {
  name: 'UsersList',
  components: {
    usersTable
  },
  data() {
    return {
      users: [],
      filteredUsers: [],
      genderFilter: undefined,
      ageFilter: undefined,
      nationalityFilter: undefined,
      isLoading: false,
      genderOptions: [
        { text: 'Hombre', value: 'male' },
        { text: 'Mujer', value: 'female' }
      ]
    }
  },
  created() {
    if (this.$store.state.usersList.length) {
      this.filteredUsers = this.$store.state.usersList;
    } else {
      this.loadUsers();
    }
  },
  methods: {
    loadUsers() {
      this.isLoading = true;
      usersService.listUsers({ queryParams: `?seed=foobar&results=100` })
        .then(response => {
          const formattedUsers = this.formatUsers(response.data.results);
          this.$store.commit('setUsersList', formattedUsers);
          this.filteredUsers = formattedUsers;
        })
        .catch(error => console.error(error))
        .finally(() => this.isLoading = false);
    },
    formatUsers(users) {
      return users.map(user => ({
        ...user,
        name: `${user.name.first} ${user.name.last}`,
        nationality: user.nat,
        birthDate: moment(user.dob.date).format('DD-MM-YYYY'),
        registrationDate: moment(user.registered.date).format('DD-MM-YYYY'),
        genderTranslation: user.gender === 'male' ? 'Hombre' : 'Mujer'
      }));
    },
    handleUserSelection(user) {
      this.$router.push({ name: 'UsersDetails', params: { email: user.email } });
    },
    applyFilters() {
      this.filteredUsers = this.$store.state.usersList.filter(user => {
        if (this.genderFilter && this.genderFilter.length && !this.genderFilter.includes(user.gender)) {
          return false;
        }
        if (this.nationalityFilter && this.nationalityFilter !== user.nationality) {
          return false;
        }
        if (this.ageFilter && this.ageFilter != user.dob.age) {
          return false;
        }
        return true;
      });
    },
    handleFavoritesList() {
      this.$router.push({ name: 'UsersFavorites' });
    }
  }
}
