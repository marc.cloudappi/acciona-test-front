import moment from 'moment';
import { latLng } from "leaflet";
import { LMap, LTileLayer, LMarker, LPopup, LTooltip } from "vue2-leaflet";

export default {
  name: 'UsersDetails',
  components: {
    LMap,
    LTileLayer,
    LMarker,
    LPopup,
    LTooltip
  },
  data() {
    return {
      user: undefined,
      zoom: 5,
      url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      mapOptions: {
        zoomSnap: 0.5
      }
    }
  },
  created() {
    this.user = this.$store.getters.getUserByEmail(this.$route.params.email);
  },
  methods: {
    formatUser(user) {
      return {
        ...user,
        name: `${user.name.first} ${user.name.last}`,
        nationality: user.nat,
        birthDate: moment(user.dob.date).format('DD-MM-YYYY'),
        registrationDate: moment(user.registered.date).format('DD-MM-YYYY'),
        genderTranslation: user.gender === 'male' ? 'Hombre' : 'Mujer'
      };
    },
    toggleFavorite() {
      if (this.$store.getters.isUserFavorite(this.user)) {
        this.$store.commit('removeFavoriteUser', this.user);
      } else {
        this.$store.commit('addFavoriteUser', this.user);
      }
    }
  },
  computed: {
    userLocation() {
      return this.user ? latLng(
        this.user.location.coordinates.latitude,
        this.user.location.coordinates.longitude
      ) : undefined;
    },
    userStreet() {
      return this.user ?
        `${this.user.location.street.name}, ${this.user.location.street.number}`
        : undefined;
    },
    isUserFavorite() {
      return this.$store.getters.isUserFavorite(this.user);
    }
  }
}
