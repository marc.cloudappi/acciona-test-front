import usersService from '../../../services/usersService';
import usersTable from '../../../components/users/usersTable.vue';

export default {
  name: 'UsersFavorites',
  components: {
    usersTable
  },
  data() {
    return {
      showListCreation: false,
      isProcessing: false,
      newListName: ''
    }
  },
  methods: {
    handleUserSelection(user) {
      this.$router.push({ name: 'UsersDetails', params: { email: user.email } });
    },
    handleNewFavoritesList() {
      if (!this.newListName) {
        return;
      }
      this.isProcessing = true;
      usersService.createFavoritesList(this.newListName, this.$store.state.favoriteUsersList)
        .then(() => {
          this.showListCreation = false;
          this.newListName = '';
        })
        .catch(error => console.error(error))
        .finally(() => this.isProcessing = false);
    }
  },
  computed: {
    exportedData() {
      return this.$store.state.favoriteUsersList.map(user => ({
        Género: user.genderTranslation,
        Nombre: user.name,
        Email: user.email,
        Nacionalidad: user.nationality,
        'Fecha de nacimiento': user.birthDate,
        'Fecha de registro': user.registrationDate
      }));
    }
  }
}
