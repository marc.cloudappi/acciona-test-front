import axios from 'axios';

const RANDOM_USER_BASE_URL = 'https://randomuser.me/api/';
const USERS_API_BASE_URL = 'http://localhost:3000/';

const listUsers = async(config) => {
  return await axios.get(`${RANDOM_USER_BASE_URL}${config.queryParams || ''}`);
};

const createFavoritesList = async(name, users) => {
  return await axios.post(`${USERS_API_BASE_URL}favorites`, { name, users });
};

export default {
  listUsers,
  createFavoritesList
}
