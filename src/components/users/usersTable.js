export default {
  name: 'UsersTable',
  props: {
    users: Array,
    isLoading: Boolean
  },
  data() {
    return {
      headers: [
        { text: 'Genero', value: 'genderTranslation' },
        { text: 'Nombre', value: 'name' },
        { text: 'Email', value: 'email' },
        { text: 'Nacionalidad', value: 'nationality' },
        { text: 'Fecha de nacimiento', value: 'birthDate' },
        { text: 'Fecha de registro', value: 'registrationDate' }
      ]
    }
  },
  methods: {
    handleUserSelection(user) {
      this.$emit('user-selected', user);
    }
  }
}
