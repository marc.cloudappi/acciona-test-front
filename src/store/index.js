import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    usersList: [],
    favoriteUsersList: []
  },
  mutations: {
    setUsersList(state, usersList) {
      state.usersList = usersList;
    },
    addFavoriteUser(state, user) {
        state.favoriteUsersList = [...state.favoriteUsersList, user];
    },
    removeFavoriteUser(state, user) {
      state.favoriteUsersList = state.favoriteUsersList.filter(favoriteUser =>
        favoriteUser.email !== user.email);
    },
  },
  getters: {
    getUserByEmail: state => email => {
      return state.usersList.find(user => user.email === email);
    },
    isUserFavorite: state => user => {
      return state.favoriteUsersList.some(favoriteUser =>
        favoriteUser.email === user.email);
    }
  }
});

export default store;
